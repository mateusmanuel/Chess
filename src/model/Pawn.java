package model;

import java.awt.Point;
import java.util.ArrayList;

public class Pawn extends Piece {

	private ArrayList<Point> moves;
	
	public Pawn(String imagePath, Point piecePosition, Team team) {
		super(imagePath, piecePosition, team);
	}
	
	public Pawn(Team team) {
		super(team);
	}

	public Pawn(String imagePath) {
		super(imagePath); 
	}

	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		this.moves = new ArrayList<Point>();
			
			if (getImagePath().contains("White")) {
				moves.add(new Point(x, y-1));
				if (y == 6)			
					moves.add(new Point(x, y-2));
			}
			if (getImagePath().contains("Brown")){ 
				moves.add(new Point(x, y+1));
				if (y == 1)			
					moves.add(new Point(x, y+2));
			}
			
		return this.moves;
	}
}
