package model;

import java.awt.Point;
import java.util.ArrayList;

public abstract class Piece {

	public static final int CHESSBOARD_ROW = 8;
	public static final int CHESSBOARD_COL = 8;

	private String imagePath;
	private Point piecePosition;
	private Team team;

	public static enum Team {
		UP_TEAM, DOWN_TEAM;
	}
	
	public Piece(Team team) {
		super();
		this.team = team;
	}

	public Piece(String imagePath) {
		super();
		this.imagePath = imagePath;
	}

	public Piece(String imagePath, Point piecePosition) {
		super();
		this.imagePath = imagePath;
		this.piecePosition = piecePosition;
	}

	public Piece(String imagePath, Point piecePosition, Team team) {
		this.imagePath = imagePath;
		this.piecePosition = piecePosition;

		this.team = team;
	}

	public abstract ArrayList<Point> getMoves(int x, int y);

	public boolean pieceIsOnMyTeam(Piece piece) {
		return this.team == piece.getTeam();
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Point getPiecePosition() {
		return piecePosition;
	}

	public void setPiecePosition(Point piecePosition) {
		this.piecePosition = piecePosition;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
}
