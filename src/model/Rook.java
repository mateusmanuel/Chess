package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rook extends Piece {

	private ArrayList<Point> moves;

	public Rook(Team team) {
		super(team);
	}
	
	public Rook(String imagePath) {
		super(imagePath); 
	}

	@Override
	public ArrayList<Point> getMoves(int x, int y) {

		this.moves = new ArrayList<Point>();
		
//		if (getImagePath().contains("Brown")) {
//			for (int i = 1; i < CHESSBOARD_COL; i++) {
//				if (i != y) {
//					this.moves.add(new Point(x, i));
//				}
//			}
//			
//			for (int i = 1; i < CHESSBOARD_COL; i++) {
//				if (i != x) {
//					this.moves.add(new Point(i, y));
//				}
//			}
//		}
//		else if (getImagePath().contains("White")) {
//			for (int i = CHESSBOARD_COL - 1; i >= 0; i--) {
//				if (i != y) {
//					this.moves.add(new Point(x, i));
//				}
//			}
//
//			for (int i = CHESSBOARD_COL - 1; i >= 0; i--) {
//				if (i != x) {
//					this.moves.add(new Point(i, y));
//				}
//			}
//		}
		
		for (int i = 1; i < CHESSBOARD_ROW; i++) {
			moves.add(new Point(x, y+i));
		}
		
		for (int i = 1; i < CHESSBOARD_ROW; i++) {
			moves.add(new Point(x, y-i));
		}		
		
		for (int i = 1; i < CHESSBOARD_ROW; i++) {
			moves.add(new Point(x+i, y));
		}
		
		for (int i = 1; i < CHESSBOARD_ROW; i++) {
			moves.add(new Point(x-i, y));
		}
		
		return this.moves;
	}
}
