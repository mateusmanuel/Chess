package model;

import java.awt.Point;
import java.util.ArrayList;

public class Knight extends Piece {

	private ArrayList<Point> moves;
	
	public Knight(String imagePath, Point piecePosition, Team team) {
		super(imagePath, piecePosition, team);
	}

	public Knight(String imagePath) {
		super(imagePath); 
	}
	
	public Knight(Team team) {
		super(team);
	}
	
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		this.moves = new ArrayList<Point>();
		
		moves.add(new Point(x+1, y+2));
		moves.add(new Point(x-1, y+2));
		moves.add(new Point(x-1, y-2));
		moves.add(new Point(x+1, y-2));
		moves.add(new Point(x+2, y+1));
		moves.add(new Point(x+2, y-1));
		moves.add(new Point(x-2, y+1));
		moves.add(new Point(x-2, y-1));
		
		return moves;
	}

}
