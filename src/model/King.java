package model;

import java.awt.Point;
import java.util.ArrayList;

public class King extends Piece {
	
	public King(String imagePath, Point piecePosition, Team team) {
		super(imagePath, piecePosition, team);
	}

	public King(String imagePath) {
		super(imagePath); 
	}
	
	public King(Team team) {
		super(team);
	}
	
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		
		ArrayList<Point> moves = new ArrayList<Point>();
		
		moves.add(new Point(x+1, y+1));
		moves.add(new Point(x-1, y-1));
		moves.add(new Point(x+1, y-1));
		moves.add(new Point(x-1, y+1));
		moves.add(new Point(x, y+1));
		moves.add(new Point(x+1, y));
		moves.add(new Point(x, y-1));
		moves.add(new Point(x-1, y));
		
		return moves;
	}
}
