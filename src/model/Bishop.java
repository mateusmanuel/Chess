package model;

import java.awt.Point;
import java.util.ArrayList;

public class Bishop extends Piece {
	
	private ArrayList<Point> moves;
	
	public Bishop(String imagePath, Point piecePosition, Team team) {
		super(imagePath, piecePosition, team);
	}

	public Bishop(String imagePath) {
		super(imagePath); 
	}
	
	public Bishop(Team team) {
		super(team);
	}
	
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		this.moves = new ArrayList<Point>();
	
		for (int i = 1; i < CHESSBOARD_ROW; i++) {
			moves.add(new Point(x+i, y+i));
		}
		
		for (int i = 1; i < CHESSBOARD_ROW; i++) {
			moves.add(new Point(x-i, y-i));
		}		
		
		for (int i = 1; i < CHESSBOARD_ROW; i++) {
			moves.add(new Point(x+i, y-i));
		}
		
		for (int i = 1; i < CHESSBOARD_ROW; i++) {
			moves.add(new Point(x-i, y+i));
		}
		
		return moves;
	}

}
