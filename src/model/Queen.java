package model;

import java.awt.Point;
import java.util.ArrayList;

public class Queen extends Piece {

	private ArrayList<Point> moves;
	
	public Queen(String imagePath, Point piecePosition, Team team) {
		super(imagePath, piecePosition, team);
	}

	public Queen(String imagePath) {
		super(imagePath); 
	}
	
	public Queen(Team team) {
		super(team);
	}
	
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		this.moves = new ArrayList<Point>();
		
		Piece bishopMoves = new Bishop("");
		Piece rookMoves = new Rook("");
		
		moves.addAll(bishopMoves.getMoves(x, y));
		moves.addAll(rookMoves.getMoves(x, y));
		
		return moves;
	}
}
