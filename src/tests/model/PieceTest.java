package tests.model;

import java.awt.Point;
import java.util.ArrayList;

import model.Bishop;
import model.King;
import model.Knight;
import model.Pawn;
import model.Piece;
import model.Piece.Team;
import model.Queen;
import model.Rook;

import org.junit.Assert;
import org.junit.Test;

import tests.helper.MovesTestHelper;

public class PieceTest {
	private static final Class<?> CLASSE_PAWN = Pawn.class;
	private static final Class<?> CLASSE_ROOK = Rook.class;
	private static final Class<?> CLASSE_KNIGHT = Knight.class;
	private static final Class<?> CLASSE_BISHOP = Bishop.class;
	private static final Class<?> CLASSE_KING = King.class;
	private static final Class<?> CLASSE_QUEEN = Queen.class;

	@Test
	public void testMovePawn() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_PAWN, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getPawnMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveTower() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_ROOK, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getTowerMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveHorse() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_KNIGHT, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getHorseMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveBishop() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_BISHOP, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getBishopMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveKing() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_KING, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getKingMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveQueen() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;

		Piece piece = newPieceInstance(CLASSE_QUEEN, Team.UP_TEAM);

		ArrayList<Point> moves = new ArrayList<Point>();

		moves.addAll(MovesTestHelper.getTowerMoves());
		moves.addAll(MovesTestHelper.getBishopMoves());

		assertMoves(moves, piece.getMoves(x, y));
	}

	private Piece newPieceInstance(Class<?> classPiece, Team team)
			throws Exception {
		return (Piece) classPiece.getDeclaredConstructor(Team.class)
				.newInstance(team);
	}

	private void assertMoves(ArrayList<Point> movesA, ArrayList<Point> movesB)
			throws Exception {
		if (movesA.size() != movesB.size()) {
			Assert.assertTrue(false);
			return;
		}

		for (Point point : movesA) {
			if (!movesB.contains(point)) {
				Assert.assertTrue(false);
				return;
			}
		}

		Assert.assertTrue(true);
	}
}
