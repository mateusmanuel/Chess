package control;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import model.Piece;

import model.Square;
import model.Square.SquareEventListener;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_SQUARE = new Color(0, 0, 255, 0);
		
	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;

	private Square selectedSquare;
	private ArrayList<Square> squareList;
	private ArrayList<Square> possibleMovesList;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;

		this.squareList = new ArrayList<>();
		this.possibleMovesList = new ArrayList<>();
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		square.setColor(this.colorHover);
	}

	@Override
	public void onSelectEvent(Square square) {
		int x = square.getPosition().x;
		int y = square.getPosition().y;
		System.out.println("(x " + x + ", y " + y + ")");

		if (haveSelectedCellPanel()) {
			if (!this.selectedSquare.equals(square)) {
				moveContentOfSelectedSquare(square);
			} else {
				unselectSquare(square);
			}
		} else {
			selectSquare(square);
		}
	}

	@Override
	public void onOutEvent(Square square) {
		if (this.selectedSquare != square && !possibleMovesList.contains(square)) {
			resetColor(square);
		} else {
			if(this.selectedSquare == square)
				square.setColor(this.colorSelected);
		}
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		
		if(checkPossibleMoves(square)) {
			square.setPiece(this.selectedSquare.getPiece());
			this.selectedSquare.removePiece();
			unselectSquare(square);
		}
	}

	private void selectSquare(Square square) {
		if (square.havePiece()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			this.showMoves(this.selectedSquare);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
				
		for (int i = 0; i < possibleMovesList.size(); i++) {
			resetColor(possibleMovesList.get(i));
		}
		
		possibleMovesList.clear();
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
	
	private boolean checkPossibleMoves(Square square){
		
		for(int i=0; i<possibleMovesList.size(); i++){
			if(possibleMovesList.get(i).getPosition() == square.getPosition())
				return true;
		}
		return false;
	}
	
	private boolean outOfBounds (ArrayList<Point> move, int index) {
		if(move.get(index).y > 7 || move.get(index).x > 7 || move.get(index).y < 0 || move.get(index).x < 0) {
			return true;
		}
		return false;
	}
	
	private int generatePossibleMoves (Square square, ArrayList<Point> pieceMoves, int index) {
		
		Square showSquares = getSquare(pieceMoves.get(index).x, pieceMoves.get(index).y);
		
		if(square.getPiece().getImagePath().contains("Brown")){
			if(showSquares.havePiece()){
				if(showSquares.getPiece().getImagePath().contains("Brown")){
					return 6 - index%7;
				}
				else if(showSquares.getPiece().getImagePath().contains("White")){ 
					showSquares.setColor(this.colorHover);
					possibleMovesList.add(showSquares);
					return 6 - index%7;
				}
			}
			else if(!showSquares.havePiece()){
				showSquares.setColor(this.colorHover);
				possibleMovesList.add(showSquares);
			}
		}
		else if(square.getPiece().getImagePath().contains("White")){
			if(showSquares.havePiece()){
				if(showSquares.getPiece().getImagePath().contains("White")){
					return 6 - index%7;
				}
				else if(showSquares.getPiece().getImagePath().contains("Brown")){
					showSquares.setColor(this.colorHover);
					possibleMovesList.add(showSquares);
					return 6 - index%7;
				}
			}
			else if(!showSquares.havePiece()){
				showSquares.setColor(this.colorHover);
				possibleMovesList.add(showSquares);
			}
		}
		return 0;
	}
	
	private boolean generatePossibleMovesPawn (Square square, ArrayList<Point> pieceMoves, int index) {
		int control = 0;		
		
		if(square.getPiece().getImagePath().contains("Brown P")) {
			if(square.getPosition().y+1 <= 7 && square.getPosition().x+1 <= 7) {
				
				Square diagonalSquare1 = getSquare(square.getPosition().x+1, square.getPosition().y+1);
		
				if (diagonalSquare1.havePiece() && diagonalSquare1.getPiece().getImagePath().contains("White")){
					diagonalSquare1.setColor(this.colorHover);
					possibleMovesList.add(diagonalSquare1);
					control = 1;
				}
			}
			if (square.getPosition().y+1 <= 7 && square.getPosition().x-1 >=0) {
				Square diagonalSquare2 = getSquare(square.getPosition().x-1, square.getPosition().y+1);
						
				if (diagonalSquare2.havePiece() && diagonalSquare2.getPiece().getImagePath().contains("White")){
					diagonalSquare2.setColor(this.colorHover);
					possibleMovesList.add(diagonalSquare2);
					control = 1;
				}
			}
			if (control == 1){
				return true;
			}
		}
		
		else if(square.getPiece().getImagePath().contains("White P")) {
			if(square.getPosition().y-1 >= 0 && square.getPosition().x+1 <= 7) {
				Square diagonalSquare1 = getSquare(square.getPosition().x+1, square.getPosition().y-1);
			
				if (diagonalSquare1.havePiece() && diagonalSquare1.getPiece().getImagePath().contains("Brown")){
					diagonalSquare1.setColor(this.colorSelected);
					possibleMovesList.add(diagonalSquare1);
					control = 1;
				}
			}
			if(square.getPosition().y-1 >= 0 && square.getPosition().x-1 >= 0){
				Square diagonalSquare2 = getSquare(square.getPosition().x-1, square.getPosition().y-1);
						
				if (diagonalSquare2.havePiece() && diagonalSquare2.getPiece().getImagePath().contains("Brown")){
					diagonalSquare2.setColor(this.colorSelected);
					possibleMovesList.add(diagonalSquare2);
					control = 1;
				}
			}
			if (control == 1){
				return true;
			}
		}
		return false;
	}
		
	
	private void showKingMoves(Square square){
		
		ArrayList<Point> kingMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for(int i = 0; i < kingMoves.size(); i++){
		
			if(outOfBounds(kingMoves, i)){
				continue;
			}
			generatePossibleMoves(square, kingMoves, i);
		}
	}
	
	private void showPawnMoves(Square square){
		
		ArrayList<Point> pawnMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for(int i = 0; i < pawnMoves.size(); i++){
			
			if(outOfBounds(pawnMoves, i)){
				continue;
			}
			
			if(!generatePossibleMovesPawn(square, pawnMoves, i)){
				generatePossibleMoves(square, pawnMoves, i);
			}	
		}
	}
	
	private void showBishopMoves(Square square){
		
		ArrayList<Point> bishopMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
				
		for(int i = 0; i < bishopMoves.size(); i++){
			
			if(outOfBounds(bishopMoves, i)){
				continue;
			}
			if (generatePossibleMoves(square, bishopMoves, i) != 0){
				i+=generatePossibleMoves(square, bishopMoves, i);
			}
		}
	}
	
	private void showQueenMoves(Square square){
		
		ArrayList<Point> queenMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for(int i = 0; i < queenMoves.size(); i++){

			if(outOfBounds(queenMoves, i)){
				continue;
			}
			if (generatePossibleMoves(square, queenMoves, i) != 0){
				i+=generatePossibleMoves(square, queenMoves, i);
			}
		}
	}
	
	private void showKnightMoves(Square square){
		
		ArrayList<Point> knightMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);

		for(int i = 0; i < knightMoves.size(); i++){

			if(outOfBounds(knightMoves, i)){
				continue;
			}
			
			generatePossibleMoves(square, knightMoves, i);
		}
	}

	private void showRookMoves(Square square){
		
		ArrayList<Point> rookMoves = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for(int i = 0; i < rookMoves.size(); i++){

			if(outOfBounds(rookMoves, i)){
				continue;
			}
			if (generatePossibleMoves(square, rookMoves, i) != 0){
				i+=generatePossibleMoves(square, rookMoves, i);
			}
		}
	}
	
	private void showMoves(Square square){
		
		Piece piece = square.getPiece();
		
		if(piece.getImagePath().equalsIgnoreCase("icon/White K_48x48.png") || piece.getImagePath().equalsIgnoreCase("icon/Brown K_48x48.png")) {
			showKingMoves(square);
		}
		else if (piece.getImagePath().equalsIgnoreCase("icon/White P_48x48.png") || piece.getImagePath().equalsIgnoreCase("icon/Brown P_48x48.png")) {
			showPawnMoves(square);
		}
		else if (piece.getImagePath().equalsIgnoreCase("icon/White B_48x48.png") || piece.getImagePath().equalsIgnoreCase("icon/Brown B_48x48.png")) {
			showBishopMoves(square);
		}
		else if (piece.getImagePath().equalsIgnoreCase("icon/White Q_48x48.png") || piece.getImagePath().equalsIgnoreCase("icon/Brown Q_48x48.png")) {
			showQueenMoves(square);
		}
		else if (piece.getImagePath().equalsIgnoreCase("icon/White N_48x48.png") || piece.getImagePath().equalsIgnoreCase("icon/Brown N_48x48.png")) {
			showKnightMoves(square);
		}
		else if (piece.getImagePath().equalsIgnoreCase("icon/White R_48x48.png") || piece.getImagePath().equalsIgnoreCase("icon/Brown R_48x48.png")) {
			showRookMoves(square);
		}
	}
}
