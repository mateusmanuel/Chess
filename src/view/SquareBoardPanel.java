package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.*;
import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().x;
			gridBag.gridy = square.getPosition().y;
			square.getPosition().setLocation(gridBag.gridx, gridBag.gridy);

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		String piecePath = "icon/Brown P_48x48.png";
		Piece pawnBrown = new Pawn(piecePath);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(i, 1).setPiece(pawnBrown);
		}
		
		piecePath = "icon/Brown R_48x48.png";
		Piece rookBrown = new Rook(piecePath);
		this.squareControl.getSquare(0, 0).setPiece(rookBrown);
		this.squareControl.getSquare(7, 0).setPiece(rookBrown);

		piecePath = "icon/Brown N_48x48.png";
		Piece knightBrown = new Knight(piecePath);
		this.squareControl.getSquare(1, 0).setPiece(knightBrown);
		this.squareControl.getSquare(6, 0).setPiece(knightBrown);

		piecePath = "icon/Brown B_48x48.png";
		Piece bishopBrown = new Bishop(piecePath);
		this.squareControl.getSquare(2, 0).setPiece(bishopBrown);
		this.squareControl.getSquare(5, 0).setPiece(bishopBrown);

		piecePath = "icon/Brown Q_48x48.png";
		Piece queenBrown = new Queen(piecePath);
		this.squareControl.getSquare(4, 0).setPiece(queenBrown);

		piecePath = "icon/Brown K_48x48.png";
		Piece kingBrown = new King(piecePath);
		this.squareControl.getSquare(3, 0).setPiece(kingBrown);

		piecePath = "icon/White P_48x48.png";
		Piece pawnWhite = new Pawn(piecePath);
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(i, 6).setPiece(pawnWhite);
		}
		
		piecePath = "icon/White R_48x48.png";
		Piece rookWhite = new Rook(piecePath);
		this.squareControl.getSquare(0, 7).setPiece(rookWhite);
		this.squareControl.getSquare(7, 7).setPiece(rookWhite);

		piecePath = "icon/White N_48x48.png";
		Piece knightWhite = new Knight(piecePath);
		this.squareControl.getSquare(1, 7).setPiece(knightWhite);
		this.squareControl.getSquare(6, 7).setPiece(knightWhite);

		piecePath = "icon/White B_48x48.png";
		Piece bishopWhite = new Bishop(piecePath);
		this.squareControl.getSquare(2, 7).setPiece(bishopWhite);
		this.squareControl.getSquare(5, 7).setPiece(bishopWhite);

		piecePath = "icon/White Q_48x48.png";
		Piece queenWhite = new Queen(piecePath);
		this.squareControl.getSquare(4, 7).setPiece(queenWhite);

		piecePath = "icon/White K_48x48.png";
		Piece king = new King(piecePath);
		this.squareControl.getSquare(3, 7).setPiece(king);
	}
}
